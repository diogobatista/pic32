#include <detpic32.h>

void send2displays(unsigned char);
void delay();

int main (void){

	TRISB = TRISB & 0xFC00;
	unsigned char count = 0x00000000;
	unsigned int countFlag = 0;
	
	while(1){
	
		delay(1);
		if(countFlag >= 100)
		{
		count = count + 0x1;
		countFlag = 0;
		}
		else
		{
		countFlag += 1;
		}
		send2displays(count);
		putChar(count);
		printStr("NOVO CICLO\n");
		
}
}

void send2displays(unsigned char value){

	static const unsigned char display7Scodes[] = {0x77, 0x41, 0x3B, 0x6B, 0x4D, 0x6E, 0x7E, 0x43, 0x7F, 0x6F, 0x5F, 0x7C, 0x38, 0x79, 0x3E, 0x1E};
	static unsigned char DisplayFlag = 0;

	char digit_low = display7Scodes[value & 0x0F];
	char digit_high = display7Scodes[value >> 4];


	if(DisplayFlag == 1) 
	{
	//SEND TO LOW
	
		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 0;
		LATBbits.LATB9 = 1;
		LATB = LATB | digit_low;
		DisplayFlag = 0;
		
	}else
	{
	//SEND TO HIGH

		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 1;
		LATBbits.LATB9 = 0;
		LATB = LATB | digit_high;
		DisplayFlag = 1;
	}
}

void delay(unsigned int n_intervals){
	volatile unsigned int i;

	for(; n_intervals != 0; n_intervals--)
		for(i= 9967; i!=0;i--);
}
