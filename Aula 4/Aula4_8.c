void send2displays(unsigned char value){

static const unsigned char display7Scodes[] = {0x77, 0x41, 0x3B, 0x6B, 0x4D, 0x6E, 0x7E, 0x43, 0x7F, 0x6F, 0x5F, 0x7C, 0x38, 0x79, 0x3E, 0x1E}


	//SEND TO LOW
	
	value = value & 0x0F;  // MASK
	LATB = LATB & 0xFF00;
	LATBbits.LATB8 = 0;
	LATBbits.LATB9 = 1;
	LATB = LATB | display7Scodes[value];
	
	//SEND TO HIGH

	value = value & 0xF0;  //MASK
	value = value >> 4;
	LATB = LATB & 0xFF00;
	LATBbits.LATB8 = 1;
	LATBbits.LATB9 = 0;
	LATB = LATB | display7Scodes[value];
}
