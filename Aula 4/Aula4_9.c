#include <detpic32.h>

void send2displays(unsigned char);
void delay();

int main (void){
	
	TRISB = TRISB & 0xFC00;
	unsigned char count = 0x00000000;
	
	while(1){
	
		
		count = count + 0x1;
		send2displays(count);
		putChar(count);
		printStr("NOVO CICLO\n");
		
}
}

void send2displays(unsigned char value){

	static const unsigned char display7Scodes[] = {0x77, 0x41, 0x3B, 0x6B, 0x4D, 0x6E, 0x7E, 0x43, 0x7F, 0x6F, 0x5F, 0x7C, 0x38, 0x79, 0x3E, 0x1E};
	unsigned char aux = value;

	//SEND TO LOW
	
	aux = aux & 0x0F;  // MASK
	LATB = LATB & 0xFF00;
	LATBbits.LATB8 = 0;
	LATBbits.LATB9 = 1;
	LATB = LATB | display7Scodes[aux];
	delay(200);
	
	aux = value;
	
	//SEND TO HIGH

	aux = aux & 0xF0;  //MASK
	aux = aux >> 4;
	LATB = LATB & 0xFF00;
	LATBbits.LATB8 = 1;
	LATBbits.LATB9 = 0;
	LATB = LATB | display7Scodes[aux];
	delay(200);
}

void delay(unsigned int n_intervals){
	volatile unsigned int i;

	for(; n_intervals != 0; n_intervals--)
		for(i= 9967; i!=0;i--);
}
