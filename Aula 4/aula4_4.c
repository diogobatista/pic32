#include <detpic32.h>


void delay();
void sendToHigh();
void sendToLow();


int main(void)
{
	
	TRISB = TRISB & 0xFC00;
	
	int mask = 0x1;
	
	
		
	while(1){
		
		sendToLow(mask);
		delay(200);
		sendToHigh(mask);
		delay(200);
		mask = mask << 1;
		
		if(mask > 0x80)
		mask = 0x1;
	}
	
}



void sendToLow(int segment){
	
	LATB = LATB & 0xFF00;
	LATBbits.LATB9 = 1;
	LATBbits.LATB8 = 0;
	LATB = LATB | segment;
}

void sendToHigh(int segment){
	
	LATB = LATB & 0xFF00;
	LATBbits.LATB8 = 1;
	LATBbits.LATB9 = 0;
	LATB = LATB | segment;
}

void delay(unsigned int n_intervals){
	volatile unsigned int i;

	for(; n_intervals != 0; n_intervals--)
		for(i= 9967; i!=0;i--);
}
