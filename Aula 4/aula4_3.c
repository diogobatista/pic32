#include <detpic32.h>

void main(void)
{
	
	int mode = 1;
	char aux = 'x';
	
	TRISB = (TRISB & 0xFC00);
	
	if(mode == 0)
	{
		LATBbits.LATB9=1;
		LATBbits.LATB8=0;
	} else{
		printStr("teste");
		LATBbits.LATB9=0;
		LATBbits.LATB8=1;
	}
	
	printStr("Utilize A-G e . para selecionar o segmento desejado.");
	
	while(1){
		aux = getChar();
		
		if(aux == 'a' || aux == 'A'){
			
			LATBbits.LATB0=0;
			LATBbits.LATB1=1;
			LATBbits.LATB2=0;
			LATBbits.LATB3=0;
			LATBbits.LATB4=0;
			LATBbits.LATB5=0;
			LATBbits.LATB6=0;
			LATBbits.LATB7=0; 
			
		}
		if(aux == 'b' || aux == 'B'){
			
			LATBbits.LATB0=1;
			LATBbits.LATB1=0;
			LATBbits.LATB2=0;
			LATBbits.LATB3=0;
			LATBbits.LATB4=0;
			LATBbits.LATB5=0;
			LATBbits.LATB6=0;
			LATBbits.LATB7=0; 
			
		}
		if(aux == 'c' || aux == 'C'){
			
			LATBbits.LATB0=0;
			LATBbits.LATB1=0;
			LATBbits.LATB2=0;
			LATBbits.LATB3=0;
			LATBbits.LATB4=0;
			LATBbits.LATB5=0;
			LATBbits.LATB6=1;
			LATBbits.LATB7=0; 
			
		}
		if(aux == 'd' || aux == 'D'){
			
			LATBbits.LATB0=0;
			LATBbits.LATB1=0;
			LATBbits.LATB2=0;
			LATBbits.LATB3=0;
			LATBbits.LATB4=0;
			LATBbits.LATB5=1;
			LATBbits.LATB6=0;
			LATBbits.LATB7=0; 
			
		}
		if(aux == 'e' || aux == 'E'){
			
			LATBbits.LATB0=0;
			LATBbits.LATB1=0;
			LATBbits.LATB2=0;
			LATBbits.LATB3=0;
			LATBbits.LATB4=1;
			LATBbits.LATB5=0;
			LATBbits.LATB6=0;
			LATBbits.LATB7=0; 
			
		}
		if(aux == 'f' || aux == 'F'){
			
			LATBbits.LATB0=0;
			LATBbits.LATB1=0;
			LATBbits.LATB2=1;
			LATBbits.LATB3=0;
			LATBbits.LATB4=0;
			LATBbits.LATB5=0;
			LATBbits.LATB6=0;
			LATBbits.LATB7=0; 
			
		}
		if(aux == 'g' || aux == 'G'){
			
			LATBbits.LATB0=0;
			LATBbits.LATB1=0;
			LATBbits.LATB2=0;
			LATBbits.LATB3=1;
			LATBbits.LATB4=0;
			LATBbits.LATB5=0;
			LATBbits.LATB6=0;
			LATBbits.LATB7=0; 
			
		}
		if(aux == '.'){
			
			LATBbits.LATB0=0;
			LATBbits.LATB1=0;
			LATBbits.LATB2=0;
			LATBbits.LATB3=0;
			LATBbits.LATB4=0;
			LATBbits.LATB5=0;
			LATBbits.LATB6=0;
			LATBbits.LATB7=1; 
		
		}
	
	}
	
}
