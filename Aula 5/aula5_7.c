#include <detpic32.h>


void delay(unsigned int);
void send2displays(unsigned char);

int main(void)
{
	int a;
	int aux, media=0, flag_media=0, media16;
	int flag_convAD=0;
	
	TRISB = TRISB & 0xFC00;
	
	TRISBbits.TRISB14 = 1; //Output de RB14 desactivo
	AD1PCFGbits.PCFG14 = 0; //RB14 passa a input analógico
	
	AD1CHSbits.CH0SA = 14; // RB14 passa a entrada analógica, linkada ao conversor A/D
	AD1CON2bits.SMPI = 3;  // 4 conversões consecutivas
	
	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;
	
	AD1CON3bits.SAMC = 16;
	
	AD1CON1bits.ON = 1; // ENABLE A/D CONVERTER
	
	while(1){
		

		
		flag_convAD++;
		if(flag_convAD >=10)
		{
			AD1CON1bits.ASAM = 1; // Inicio da conversao A/D
			flag_convAD = 0;
			while(IFS1bits.AD1IF == 0);
			
			unsigned int *p = (int *) (&ADC1BUF0);
			 
			for(a=0; a<16; a++)
			{
				printInt(p[a*4], 0x0004000A);
				printStr(" ");
			}
			
		aux = p[0] + p[4] + p[8] + p[12];
		aux = aux/4;
		aux = ((aux*33)+511)/1023;
		printStr("\n Tensão: ");
		printInt(aux, 10);
		
		}
		
		if(flag_media < 3)
		{
			media += aux;
			flag_media++;
		}else{
			media += aux;
			media = media / 4;
			media16 = (media/10) << 4;
			media16 = media16 | (media%10);

			media = 0;
			flag_media = 0;
		}
		
		send2displays(media16);
		printStr("\n Displays7S actualizado \n");				
		IFS1bits.AD1IF = 0;
		
	}
	
}




void send2displays(unsigned char value){

	static const unsigned char display7Scodes[] = {0x77, 0x41, 0x3B, 0x6B, 0x4D, 0x6E, 0x7E, 0x43, 0x7F, 0x6F, 0x5F, 0x7C, 0x38, 0x79, 0x3E, 0x1E};
	static unsigned char DisplayFlag = 0;

	char digit_low = display7Scodes[value & 0x0F];
	char digit_high = display7Scodes[value >> 4];


	if(DisplayFlag == 1) 
	{
	//SEND TO LOW
	
		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 0;
		LATBbits.LATB9 = 1;
		LATB = LATB | digit_low;
		DisplayFlag = 0;
		
	}else
	{
	//SEND TO HIGH

		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 1;
		LATBbits.LATB9 = 0;
		LATB = LATB | digit_high;
		LATBbits.LATB7 = 1;
		DisplayFlag = 1;
	}
}


void delay(unsigned int timex){
	volatile unsigned int i;

	for(; timex != 0; timex--)
		for(i= 1000; i!=0;i--);
}

