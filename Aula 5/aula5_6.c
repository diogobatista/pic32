#include <detpic32.h>


void delay(unsigned int);

int main(void)
{
	int a;
	int aux;
	
	TRISBbits.TRISB14 = 1; //Output de RB14 desactivo
	AD1PCFGbits.PCFG14 = 0; //RB14 passa a input analógico
	
	AD1CHSbits.CH0SA = 14; // RB14 passa a entrada analógica, linkada ao conversor A/D
	AD1CON2bits.SMPI = 3;  // 4 conversões consecutivas
	
	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;
	
	AD1CON3bits.SAMC = 16;
	
	AD1CON1bits.ON = 1; // ENABLE A/D CONVERTER
	
	while(1){
		
		delay(5000);
		
		AD1CON1bits.ASAM = 1; // Inicio da conversao A/D
		
		while(IFS1bits.AD1IF == 0);
		
		unsigned int *p = (int *) (&ADC1BUF0);
		 
		for(a=0; a<16; a++)
		{
			printInt(p[a*4], 0x0004000A);
			printStr(" ");
		}
		
		aux = p[0] + p[4] + p[8] + p[12];
		aux = aux/4;
		aux = ((aux*33)+511)/1023;
		printStr("\n Tensão: ");
		printInt(aux, 10);
			
			
		
		
		printStr("\n");
		
		IFS1bits.AD1IF = 0;
		
	}
	
}


void delay(unsigned int timex){
	volatile unsigned int i;

	for(; timex != 0; timex--)
		for(i= 1000; i!=0;i--);
}

