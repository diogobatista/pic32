#include <detpic32.h>


void delay(unsigned int);

void main(void)
{
	int timex;
	int i;
	
	TRISBbits.TRISB14 = 1; //Output de RB14 desactivo
	AD1PCFGbits.PCFG14 = 0; //RB14 passa a input analógico
	
	AD1CHSbits.CH0SA = 14; // RB14 passa a entrada analógica, linkada ao conversor A/D
	AD1CON2bits.SMPI = 0;  // 1 conversão consecutiva
	
	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;
	
	AD1CON3bits.SAMC = 16;
	
	AD1CON1bits.ON = 1; // ENABLE A/D CONVERTER
	
	while(1){
	
	//for(timex = 1; timex != 0; timex--){
		//for(i= 10000; i!=0;i--){}}
		
	delay(1000);
	
	AD1CON1bits.ASAM = 1; // Inicio da conversao A/D
	
	while(IFS1bits.AD1IF == 0){}
	
	printInt(ADC1BUF0, 0x00030010);
	printStr("\r");
	
	IFS1bits.AD1IF = 0;
		
	}
	
}


void delay(unsigned int timex){
	volatile unsigned int i;

	for(; timex != 0; timex--)
		for(i= 1000; i!=0;i--);
}

