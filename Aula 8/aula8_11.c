#include <detpic32.h>

void configureTimers(void);
void send2displays(unsigned char);
unsigned char conv_amplitude(void);
void configUart(unsigned int, char, unsigned int);
void putc(char);
void putstr(char *);

volatile unsigned char value2display = 0;
volatile unsigned int voltMin = 0xF, voltMax = 0x0;

int main(void){
	
	TRISB = TRISB & 0xFC00;

	
	TRISBbits.TRISB14 = 1; //Output de RB14 desactivo
	AD1PCFGbits.PCFG14 = 0; //RB14 passa a input analógico
	
	AD1CHSbits.CH0SA = 14; // RB14 linkada ao conversor A/D
	AD1CON2bits.SMPI = 7;  // 8 conversões consecutivas
	
	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;
	
	AD1CON3bits.SAMC = 16;
	
		//PROCESSAMENTO POR INTERRUPÇÃO 
	//#######################################################
	
	IPC6bits.AD1IP=4; // Nivel de prioridade 6 (0-7)
	IEC1bits.AD1IE=1;  // Autorizar as interrupções geradas pelo módulo A/D
	IFS1bits.AD1IF = 0; // Reset flag;
	EnableInterrupts();
	
	//#######################################################
	
	AD1CON1bits.ON = 1; // ENABLE A/D CONVERTER
	
	configureTimers();
	configUart(115200, 'N', 1);
	
	while(1){
		

		
		if((PORTE&0x30) == 0x10)
		{
			IEC0bits.T1IE = 0;
		}else{
			IEC0bits.T1IE = 1;
		}
		
		};
	
}


void _int_(27) isr_adc(void)
{
	value2display = conv_amplitude();
	
	if(value2display < voltMin)
		voltMin = value2display;
		
	if(value2display > voltMax)
		voltMax = value2display;
	

	IFS1bits.AD1IF = 0;

}


void _int_(4) isr_T1(void){
	
		AD1CON1bits.ASAM = 1;
	
		IFS0bits.T1IF = 0;
}

void _int_(12) isr_T3(void){
	
	static int counter = 0;
	
	send2displays(value2display);
	
	if(++counter == 100)
	{
		counter = 0;
		putc((value2display/16) + 48);
		putc('.');
		putc((value2display%16) + 48);
		printStr("\n");
	}
	
	IFS0bits.T3IF = 0;
	
}

unsigned char conv_amplitude(){
	
	unsigned int aux, amplitude, amplitude16;
	unsigned int *p = (int *) (&ADC1BUF0);
	aux = p[0] + p[4] + p[8] + p[12] + p[16] + p[20] + p[24] + p[28];
	aux = aux/8;
	amplitude = ((aux*33)+511)/1023;
	amplitude16 = (amplitude/10) << 4;
	amplitude16 = (amplitude%10) | amplitude16;
	return amplitude16;
}


void send2displays(unsigned char value){

	static const unsigned char display7Scodes[] = {0x77, 0x41, 0x3B, 0x6B, 0x4D, 0x6E, 0x7E, 0x43, 0x7F, 0x6F, 0x5F, 0x7C, 0x38, 0x79, 0x3E, 0x1E};
	static unsigned char DisplayFlag = 0;

	char digit_low = display7Scodes[value & 0x0F];
	char digit_high = display7Scodes[value >> 4];


	if(DisplayFlag == 1) 
	{
	//SEND TO LOW
	
		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 0;
		LATBbits.LATB9 = 1;
		LATB = LATB | digit_low;
		DisplayFlag = 0;
		
	}else
	{
	//SEND TO HIGH

		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 1;
		LATBbits.LATB9 = 0;
		LATB = LATB | digit_high;
		LATBbits.LATB7 = 1;
		DisplayFlag = 1;
	}
}


void configureTimers(){
	
	T1CONbits.TCKPS = 7;  // prescaler 256
	PR1 = 19530;
	TMR1 = 0;
	T1CONbits.ON = 1;
	
	T3CONbits.TCKPS = 2;
	PR3 = 49999;
	TMR3 = 0;
	T3CONbits.ON = 1;
	
	IFS0bits.T1IF = 0;
	IPC1bits.T1IP = 2; // Prioridade
	IEC0bits.T1IE = 1;
	
	IFS0bits.T3IF = 0;
	IPC3bits.T3IP = 2; // Prioridade
	IEC0bits.T3IE = 1;
}

// DAQUI PARA BAIXO É TUDO UART


void _int_(24) isr_uart1(void){
	
	if(U1RXREG == 'L');
	{
		printStr("\n");
		printStr("Voltagem Mínima: ");
	
		putc((voltMin/16) + '0');
		putc('.');
		putc((voltMin%16) + '0');
		
		printStr("\n");
		printStr("Voltagem Máxima: ");
		putc((voltMax/16) + '0');
		putc('.');
		putc((voltMax%16) + '0');
		printStr("\n");
		
	}
	IFS0bits.U1RXIF = 0;	
	
}


void configUart(unsigned int baudrate, char parity, unsigned int stopbits){
	
	/*printStr("Baudrate selecionada para a DETPIC32: ");
	printInt(baudrate, 10);
	
	printStr("\nParidade selecionada para a DETPIC32: ");
	putChar(parity);
	
	printStr("\nNumero de stop bits para a DETPIC32: ");
	printInt(stopbits, 10);*/
	
	if((baudrate < 115200) && (baudrate > 600)){
	U1BRG = (((20000000) + (8 * baudrate)) / (16 * baudrate)) -1;  // Para saber como cheguei aqui ir ver o guião da aula8.
	}else{
		U1BRG = 10;
	}
	
	switch(parity){           // PARITY & numero de bits por trama
		case 'N':
			U1MODEbits.PDSEL = 0;
			break;
		case 'E':
			U1MODEbits.PDSEL = 1;
			break;
		case 'O':
			U1MODEbits.PDSEL = 2;
			break;
		default:
			U1MODEbits.PDSEL = 0;
			break;	
	}
	
	switch(stopbits){         // STOPBITS
		case 1:
			U1MODEbits.STSEL = 0;
			break;
		case 2: 
			U1MODEbits.STSEL = 1;
			break;
		default:
			U1MODEbits.STSEL = 0;
			break;
	}
	
	U1MODEbits.BRGH = 0; // Factor de divisão 16
	
	U1STAbits.UTXEN = 1; // Enable transmit
	U1STAbits.URXEN = 1; // Enable recieve
	
	IEC0bits.U1RXIE = 1; // Enable recieve interrupts
	IEC0bits.U1TXIE = 0; // Disable transmit interrupts
	IFS0bits.U1RXIF = 0; // Reset Flag
	U1STAbits.URXISEL = 0;  // Gera interrupcoes quando o buffer de recepção não estiver vazio
	IPC6bits.U1IP = 4; // Prioridade (comum a toda a UART)

	U1MODEbits.ON = 1; 		
}

void putstr(char *str){
	
	int i;
	
	for(i = 0; str[i] != '\0' ; i++){
		
		putc(str[i]);
		
	}	
}

void putc(char byte2send){
	
	while(U1STAbits.UTXBF == 1);
	
	U1TXREG = byte2send;	
	
}
