#include <detpic32.h>

// "pterm -h" no terminal EXPLICA TUDO o que é preciso saber

void configUart(unsigned int, char, unsigned int);
void delay(unsigned int);
void putc(char);
void putstr(char *);

int main(void)
{
	
	configUart(1200, 'N', 1);
	printStr("\n");
	printStr("\n");
	printStr("\n");
	
	while(1){
		
		putstr("String de teste\n");
		delay(1000);
				
	}

}


void putstr(char *str){
	
	int i;
	
	for(i = 0; str[i] != '\0' ; i++){
		
		putc(str[i]);
		
	}	
}

void putc(char byte2send){
	
	while(U1STAbits.UTXBF == 1);
	
	U1TXREG = byte2send;	
	
}


void configUart(unsigned int baudrate, char parity, unsigned int stopbits){
	
	/*printStr("Baudrate selecionada para a DETPIC32: ");
	printInt(baudrate, 10);
	
	printStr("\nParidade selecionada para a DETPIC32: ");
	putChar(parity);
	
	printStr("\nNumero de stop bits para a DETPIC32: ");
	printInt(stopbits, 10);*/
	
	switch(baudrate){
		default:
			U1BRG = 10;		
		case 600:
			U1BRG = 2083;
			break;
		case 1200:
			U1BRG = 1041;
			break;
		case 2400:
			U1BRG = 520;
			break;
		case 4800:
			U1BRG = 260;
			break;
		case 9600:
			U1BRG = 130;
			break;
		case 19200:
			U1BRG = 65;
			break;
		case 38400:
			U1BRG = 32;
			break;
		case 57600:
			U1BRG = 21;
			break;
		}
	
	switch(parity){           // PARITY & numero de bits por trama
		case 'N':
			U1MODEbits.PDSEL = 0;
			break;
		case 'E':
			U1MODEbits.PDSEL = 1;
			break;
		case 'O':
			U1MODEbits.PDSEL = 2;
			break;
		default:
			U1MODEbits.PDSEL = 0;
			break;	
	}
	
	switch(stopbits){         // STOPBITS
		case 1:
			U1MODEbits.STSEL = 0;
			break;
		case 2: 
			U1MODEbits.STSEL = 1;
			break;
		default:
			U1MODEbits.STSEL = 0;
			break;
	}
	
	U1MODEbits.BRGH = 0; // Factor de divisão 16
	
	U1STAbits.UTXEN = 1; // Enable transmit
	U1STAbits.URXEN = 1; // Enable recieve

	U1MODEbits.ON = 1; 		
}


void delay(unsigned int n_intervals){
	volatile unsigned int i;

	for(; n_intervals != 0; n_intervals--)
		for(i= 6666; i!=0;i--);
}
