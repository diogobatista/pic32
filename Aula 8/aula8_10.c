#include <detpic32.h>

// "pterm -h" no terminal EXPLICA TUDO o que é preciso saber

void configUart(unsigned int, char, unsigned int);
void putc(char);
void putstr(char *);
char getc(void);

int main(void)
{
	
	TRISEbits.TRISE0 = 0;
	
	configUart(115200, 'N', 1);
	printStr("\n");
	printStr("\n");
	printStr("\n");
	EnableInterrupts();
	
	while(1);

}


void _int_(24) isr_uart1(void){
	
//	while(U1STAbits.UTXBF == 1);
	
	putc(U1RXREG);
	
	IFS0bits.U1RXIF = 0;	
	
}


char getc(void){
	
	
	U1STAbits.OERR = 0;
	while(U1STAbits.URXDA == 0);
	
	if(U1STAbits.FERR == 1 || U1STAbits.PERR == 1){
	return '0'; }
	
	return U1RXREG;
}


void putstr(char *str){
	
	int i;
	
	for(i = 0; str[i] != '\0' ; i++){
		
		putc(str[i]);
		
	}	
}

void putc(char byte2send){
	
	while(U1STAbits.UTXBF == 1);
	
	U1TXREG = byte2send;	
	
}


void configUart(unsigned int baudrate, char parity, unsigned int stopbits){
	
	/*printStr("Baudrate selecionada para a DETPIC32: ");
	printInt(baudrate, 10);
	
	printStr("\nParidade selecionada para a DETPIC32: ");
	putChar(parity);
	
	printStr("\nNumero de stop bits para a DETPIC32: ");
	printInt(stopbits, 10);*/
	
	if((baudrate < 115200) && (baudrate > 600)){
	U1BRG = (((20000000) + (8 * baudrate)) / (16 * baudrate)) -1;  // Para saber como cheguei aqui ir ver o guião da aula8.
	}else{
		U1BRG = 10;
	}
	
	switch(parity){           // PARITY & numero de bits por trama
		case 'N':
			U1MODEbits.PDSEL = 0;
			break;
		case 'E':
			U1MODEbits.PDSEL = 1;
			break;
		case 'O':
			U1MODEbits.PDSEL = 2;
			break;
		default:
			U1MODEbits.PDSEL = 0;
			break;	
	}
	
	switch(stopbits){         // STOPBITS
		case 1:
			U1MODEbits.STSEL = 0;
			break;
		case 2: 
			U1MODEbits.STSEL = 1;
			break;
		default:
			U1MODEbits.STSEL = 0;
			break;
	}
	
	U1MODEbits.BRGH = 0; // Factor de divisão 16
	
	U1STAbits.UTXEN = 1; // Enable transmit
	U1STAbits.URXEN = 1; // Enable recieve
	
	IEC0bits.U1RXIE = 1; // Enable recieve interrupts
	IEC0bits.U1TXIE = 0; // Disable transmit interrupts
	IFS0bits.U1RXIF = 0; // Reset Flag
	U1STAbits.URXISEL = 0;  // Gera interrupcoes quando o buffer de recepção não estiver vazio
	IPC6bits.U1IP = 4; // Prioridade (comum a toda a UART)

	U1MODEbits.ON = 1; 		
}


