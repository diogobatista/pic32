// i2c.h
#ifndef I2C_H
#define I2C_H

// Declare symbols here (READ, WRITE, ...)
#define READ 1
#define WRITE 0
#define ACK 0
#define NACK 1
#define ADDR_WR ((SENS_ADDRESS << 1) | WRITE)
#define ADDR_RD ((SENS_ADDRESS << 1) | READ)
#define SENS_ADDRESS 0x4D 			// device dependent
#define TC74_CLK_FREQ 100000 			// 100 KHz
#define RTR 0 					// Read temperature command


// Declare function prototypes here

void i2c1_init(unsigned int);
void i2c1_start(void);
void i2c1_stop(void);
int i2c1_send(unsigned char);
char i2c1_recieve(char);

#endif
