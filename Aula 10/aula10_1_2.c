#include <detpic32.h>
#include "i2c.h"


int getTemperature(void);

int main(void){

	int ack;
	i2c1_init(TC74_CLK_FREQ);


	while(1){
		
		ack = getTemperature();
		
		if(ack == 1)
			break;
	}
	
	while(1);

}


int getTemperature(){
	
		int ack;
		char temperature;
	
		i2c1_start();
	
		ack = i2c1_send(ADDR_WR);
		
		ack = ack | i2c1_send(RTR);
		
		i2c1_start();
		
		ack = ack | i2c1_send(ADDR_RD);
	
		/*if(ack != 0){
			i2c1_stop();
			printStr("ERRO, VOU SAIR");
			break;
		}*/
		
		temperature = i2c1_recieve(NACK);
	
		i2c1_stop();
		
		printStr("Temperatura: ");
		printInt10(temperature);
		printStr("\r");
		
		resetCoreTimer();
		
		while(readCoreTimer() < 250);
		
		return ack;
	
}

void i2c1_init(unsigned int clock_freq){

	I2C1BRG = 1001;
	I2C1CONbits.ON = 1;
}


void i2c1_start(void){
	
	I2C1CONbits.SEN = 1;
	
	while(I2C1CONbits.SEN == 1);
	
}

void i2c1_stop(void){
	
	while(((I2C1CON) & (0x001F)) != 0);

	I2C1CONbits.PEN = 1;
	
	while(I2C1CONbits.PEN == 1);
	
}


int i2c1_send(unsigned char value){
	
	I2C1TRN = value;
	while(I2C1STATbits.TRSTAT ==1);
	
	return I2C1STATbits.ACKSTAT;	
	
}

char i2c1_recieve(char ack_bit){

	while(((I2C1CON) & (0x001F)) != 0);

	I2C1CONbits.RCEN = 1;

	while(I2C1STATbits.RBF == 0);
	
	I2C1CONbits.ACKDT = (ack_bit ? 1:0);

	I2C1CONbits.ACKEN = 1;
	
	while(I2C1CONbits.ACKEN == 1);

	return I2C1RCV; 
}
