#include <detpic32.h>

#define RDSR 0x05		//Read STATUS register
#define WRSR 0x01		// Write STATUS register
#define WRITE 0x02		//Write data to memory array beggining at selected address
#define READ 0x03		//Read data from memory array beggining at selected adress
#define WREN 0x06		//Set the WRITE ENABLE latch
#define WRDI 0x04		//Reset the WRITE ENABLE latch


void spi2_setClock(unsigned int);
void spi2_init(void);
char eeprom_readStatus(void);
void eeprom_writeStatusCommand(char);
void eeprom_writeData(int, char);
char eeprom_readData(int);

int main(void)
{	
	char temp, value;
	int adress, i;
	spi2_init();
	spi2_setClock(500000);
	
	while(1)
	{

	printStr("Insira um caracter: ");
	temp = getChar();
	printStr("\n");
	
	
	if(temp == 'R')
		{
		printStr("Insira um endereço a ler de memória: ");
		adress = readInt(16);
		printStr("\n");
		printStr("Valor contido nesse endereço:");
		putChar(eeprom_readData(adress));
		printStr("\n");
		}
	
	if(temp == 'W')
		{
		
		printStr("Insira um endereço para gravar em memória: ");
		adress = readInt(16);
		printStr("\n");
	
		printStr("Insira um caracter para gravar em memória: ");
		value = getChar();
		printStr("\n");
		
		for(i = 0; i<=16; i++){
		
			eeprom_writeData(adress+i, value+i);
		
			}
			
		printInt(adress, 16);
		printStr(": ");
		
		for(i = 0; i<=16; i++)
			{
			
			printInt(eeprom_readData(adress+i), 0x00000A0);
			putChar(' ');
			
			}
			printStr("\n");
		}
	
	}
	
}

void spi2_setClock(unsigned int clock_freq){
	
	SPI2BRG = (20000000 + clock_freq) / (2 * clock_freq) - 1;	
	
}

void spi2_init(void){
	
	volatile char trash; 
	
	SPI2CONbits.ON = 0; // Disable SPI2 module
	
	SPI2CONbits.CKP = 0; // Configure clock idle state as logic level 0
	SPI2CONbits.CKE = 1; // Configure the clock active transition: from active state to idle state
	
	SPI2CONbits.SMP = 0; // Configure SPI data input sample phase bit (middle of data output time)
	
	SPI2CONbits.MODE32 = 0; // Configure word length (8 bits)
	SPI2CONbits.MODE16 = 0; 
	
	SPI2CONbits.ENHBUF = 1; // Enable enhanced buffer mode
	
	SPI2CONbits.MSSEN = 1; // Enable slave select support
	
	SPI2CONbits.MSTEN = 1; // Enable Master Mode
	
	while(SPI2STATbits.SPIRBE == 0)  // Clear RX FIFO
		trash = SPI2BUF;
		
	SPI2STATbits.SPIROV = 0; // Clear overflow error flag
	
	SPI2CONbits.ON = 1; // Enable SPI2 module
	
}

char eeprom_readStatus(void){
	
	volatile char trash;
	
	while(SPI2STATbits.SPIRBE == 0)  // Clear RX FIFO
		trash = SPI2BUF;
	
	SPI2STATbits.SPIROV = 0; // Clear overflow error flag
	
	SPI2BUF = RDSR;	// Send RDSR command
	SPI2BUF = 0; // Send anything so that EEPROM clocks data into SO
	
	while(SPI2STATbits.SPIBUSY); // Wait while SPI module is working
	
	trash = SPI2BUF; // First char recieved is garbage
	return SPI2BUF; // Second received is the STATUS value
	
}

void eeprom_writeStatusCommand(char command){
	
	while(eeprom_readStatus() & 0x01); // Wait while WIP is true (write in progress)
	
	SPI2BUF = command;
	
	while(SPI2STATbits.SPIBUSY);
	
	
}

void eeprom_writeData(int adress, char value){
	
	adress = adress & 0x1FF; //Apply a mask to limit adress to 9 bits
	
	while(eeprom_readStatus() & 0x01); // Read STATUS and wait while WIP is true(write in progress)
	
	eeprom_writeStatusCommand(WREN); // Activate WEL bit in status register
	
	SPI2BUF = WRITE | ((adress & 0x100) >> 5); //Copy write command and A8 adress bit to the TX FIFO;
	
	SPI2BUF = (adress & 0xFF); //Copy adress (8LSBits to the TX FIFO)
	
	SPI2BUF = value; //Copy value to the TX FIFO
	
	while(SPI2STATbits.SPIBUSY); 
	
	
}

char eeprom_readData(int adress){

	volatile char trash;
	
	while(SPI2STATbits.SPIRBE == 0)  // Clear RX FIFO
		trash = SPI2BUF;
	
	SPI2STATbits.SPIROV = 0; //Clear overflow error flag bit
	
	adress = adress & 0x1FF; //Limit address to 9 bits

	while(eeprom_readStatus() & 0x1); //Read STATUS and wait while WIP is true (write in progress)
	
	SPI2BUF = READ | ((adress & 0x0100) >>5); //Copy READ command and A8 adress bit to the TX FIFO 
	
	SPI2BUF = (adress & 0xFF); //Copy adress (8 LSB) to the TX FIFO
	
	SPI2BUF = 0; // Copy any value to the TX FIFO
	
	while(SPI2STATbits.SPIBUSY); //Wait while SPI module is working
	
	trash = SPI2BUF; // Read and discard 2 characters from RX FIFO (use "trash" variable)
	trash = SPI2BUF;
	
	return SPI2BUF; //Read RX FIFO and return the corresponding value
}
