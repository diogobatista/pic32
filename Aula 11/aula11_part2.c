#include <detpic32.h>
#include "eeprom.h"

void i2c1_init(unsigned int);
void i2c1_start(void);
void i2c1_stop(void);
int i2c1_send(unsigned char);
char i2c1_recieve(char);


#define I2C_READ 1
#define I2C_WRITE 0
#define I2C_ACK 0
#define I2C_NACK 1

#define ADDR_WR ((SENS_ADDRESS << 1) | I2C_WRITE)
#define ADDR_RD ((SENS_ADDRESS << 1) | I2C_READ)
#define SENS_ADDRESS 0x4D // device dependent
#define TC74_CLK_FREQ  100000 // 100 KHz
#define RTR 0 // Read temperature command
          
           





int main(void){

	int ack, temperature, i=0;
	i2c1_init(TC74_CLK_FREQ);
	char temperaturec;


	while(1){
		
		resetCoreTimer();
		
		i2c1_start();
	
		ack = i2c1_send(ADDR_WR);
		
		ack = ack | i2c1_send(RTR);
		
		i2c1_start();
		
		ack = ack | i2c1_send(ADDR_RD);
	
		if(ack != 0){
			i2c1_stop();
			printStr("ERRO, VOU SAIR");
			break;
		}
		
		temperature = i2c1_recieve(I2C_NACK);
	
		i2c1_stop();
		
		printStr("Temperatura: ");
		printInt10(temperature);
		printStr("\r");
		
		temperaturec = temperature & 0xFFFF;
		
		comDrv_writeData(0, i+1)
		comDrv_writeData(0x002 + i, temperaturec)
		
		
		while(readCoreTimer() < 5000000);
		
	}
	
	while(1);
	

}


void i2c1_init(unsigned int clock_freq){

	I2C1BRG = 1001;
	I2C1CONbits.ON = 1;
}


void i2c1_start(void){
	
	I2C1CONbits.SEN = 1;
	
	while(I2C1CONbits.SEN == 1);
	
}

void i2c1_stop(void){
	
	while(((I2C1CON) & (0x001F)) != 0);

	I2C1CONbits.PEN = 1;
	
	while(I2C1CONbits.PEN == 1);
	
}


int i2c1_send(unsigned char value){
	
	I2C1TRN = value;
	while(I2C1STATbits.TRSTAT ==1);
	
	return I2C1STATbits.ACKSTAT;	
	
}

char i2c1_recieve(char ack_bit){

	while(((I2C1CON) & (0x001F)) != 0);

	I2C1CONbits.RCEN = 1;

	while(I2C1STATbits.RBF == 0);
	
	I2C1CONbits.ACKDT = (ack_bit ? 1:0);

	I2C1CONbits.ACKEN = 1;
	
	while(I2C1CONbits.ACKEN == 1);

	return I2C1RCV; 
}
