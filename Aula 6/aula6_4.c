#include <detpic32.h>

void delay(unsigned int);
void send2displays(unsigned char);
unsigned char conv_amplitude(void);

volatile unsigned char value2display = 0;

int main(void){
	
	int i = 0;
	
	TRISB = TRISB & 0xFC00;
	
	TRISBbits.TRISB14 = 1; //Output de RB14 desactivo
	AD1PCFGbits.PCFG14 = 0; //RB14 passa a input analógico
	
	AD1CHSbits.CH0SA = 14; // RB14 linkada ao conversor A/D
	AD1CON2bits.SMPI = 7;  // 8 conversões consecutivas
	
	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;
	
	AD1CON3bits.SAMC = 16;
	
		//PROCESSAMENTO POR INTERRUPÇÃO 
	//#######################################################
	
	IPC6bits.AD1IP=4; // Nivel de prioridade 6 (0-7)
	IEC1bits.AD1IE=1;  // Autorizar as interrupções geradas pelo módulo A/D
	IFS1bits.AD1IF = 0; // Reset flag;
	EnableInterrupts();
	
	//#######################################################
	
	AD1CON1bits.ON = 1; // ENABLE A/D CONVERTER
	
	
	
	while(1)
	{

		delay(10);

		if (i >= 25)
		{
			i = 0;
			AD1CON1bits.ASAM = 1;
		}
		i++;
		putChar('.');
		send2displays(value2display);
		
	}
}


void _int_(27) isr_adc(void)
{
	value2display = conv_amplitude();
	AD1CON1bits.ASAM = 1;
	IFS1bits.AD1IF = 0;

}


unsigned char conv_amplitude(){
	
	unsigned int aux, amplitude, amplitude16;
	unsigned int *p = (int *) (&ADC1BUF0);
	aux = p[0] + p[4] + p[8] + p[12] + p[16] + p[20] + p[24] + p[28];
	aux = aux/8;
	amplitude = ((aux*33)+511)/1023;
	amplitude16 = (amplitude/10) << 4;
	amplitude16 = (amplitude%10) | amplitude16;
	return amplitude16;
}


void send2displays(unsigned char value){

	static const unsigned char display7Scodes[] = {0x77, 0x41, 0x3B, 0x6B, 0x4D, 0x6E, 0x7E, 0x43, 0x7F, 0x6F, 0x5F, 0x7C, 0x38, 0x79, 0x3E, 0x1E};
	static unsigned char DisplayFlag = 0;

	char digit_low = display7Scodes[value & 0x0F];
	char digit_high = display7Scodes[value >> 4];


	if(DisplayFlag == 1) 
	{
	//SEND TO LOW
	
		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 0;
		LATBbits.LATB9 = 1;
		LATB = LATB | digit_low;
		DisplayFlag = 0;
		
	}else
	{
	//SEND TO HIGH

		LATB = LATB & 0xFF00;
		LATBbits.LATB8 = 1;
		LATBbits.LATB9 = 0;
		LATB = LATB | digit_high;
		LATBbits.LATB7 = 1;
		DisplayFlag = 1;
	}
}


void delay(unsigned int timex){
	volatile unsigned int i;

	for(; timex != 0; timex--)
		for(i= 6666; i!=0;i--);
}
