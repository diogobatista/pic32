#include <detpic32.h>


void delay(unsigned int);

int main(void)
{

	TRISBbits.TRISB14 = 1; //Output de RB14 desactivo
	AD1PCFGbits.PCFG14 = 0; //RB14 passa a input analógico

	AD1CHSbits.CH0SA = 14; // RB14 passa a entrada analógica, linkada ao conversor A/D
	AD1CON2bits.SMPI = 0;  // 1 conversão consecutiva

	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;

	AD1CON3bits.SAMC = 16;

	//PROCESSAMENTO POR INTERRUPÇÃO
	//#######################################################

	IPC6bits.AD1IP=6; // Nivel de prioridade 6 (0-7)
	IEC1bits.AD1IE=1;  // Autorizar as interrupções geradas pelo módulo A/D
	
	EnableInterrupts();

	//#######################################################


	AD1CON1bits.ON = 1; // ENABLE A/D CONVERTER

	AD1CON1bits.ASAM = 1;

	while(1){

	}

}


void _int_(27) isr_adc(void)
{
	printStr("\r");
	printInt(ADC1BUF0, 0x0000000A);
	IFS1bits.AD1IF = 0;
	AD1CON1bits.ASAM = 1;

}

void delay(unsigned int timex){
	volatile unsigned int i;

	for(; timex != 0; timex--)
		for(i= 1000; i!=0;i--);
}
