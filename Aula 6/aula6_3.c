#include <detpic32.h>


void delay(unsigned int);

void main(void)
{
	
	TRISBbits.TRISB14 = 1; //Output de RB14 desactivo
	TRISEbits.TRISE0 = 0; //RE0 COMO SAIDA !!!!!!
	AD1PCFGbits.PCFG14 = 0; //RB14 passa a input analógico
	
	AD1CHSbits.CH0SA = 14; // RB14 passa a entrada analógica, linkada ao conversor A/D
	AD1CON2bits.SMPI = 0;  // 1 conversão consecutiva
	
	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;
	
	AD1CON3bits.SAMC = 16;
	
		//PROCESSAMENTO POR INTERRUPÇÃO 
	//#######################################################
	
	IPC6bits.AD1IP=6; // Nivel de prioridade 6 (0-7)
	IEC1bits.AD1IE=1;  // Autorizar as interrupções geradas pelo módulo A/D
	EnableInterrupts();
	
	//#######################################################
	
	
	AD1CON1bits.ON = 1; // ENABLE A/D CONVERTER
	
	AD1CON1bits.ASAM = 1; // Inicio da conversao A/D
	
	while(1){
		
	

	printStr("\r");
	
		
	}
	
}


void _int_(27) isr_adc(void){
	LATEbits.LATE0 = 0;
	printInt(ADC1BUF0, 0x0003000A);
	LATEbits.LATE0 = 1; 
	AD1CON1bits.ASAM = 1;
	IFS1bits.AD1IF = 0;
							// Após as medições no osciloscopio serem efectuadas concluímos que o periodo de conversão + latência é de 90 microsegundos (us)   (O DE CONVERSÃO APENAS É DE 3.6 MICROSEGUNDOS) 
							// Ou seja, latência = 90 - 3.6 = 86.4 microsegundos
}						  // E o período de retorno à função principal é de 1.25 microsegundos (us)
							// Tempo de Overhead = latencia + periodo de retorno = 86.4 + 1.25 = 87.65 microsegundos
							// Latência é o tempo demorado pelo programa a entrar na RSI desde que a interrupção foi pedida pelo conversor

void delay(unsigned int timex){
	volatile unsigned int i;

	for(; timex != 0; timex--)
		for(i= 1000; i!=0;i--);
}

