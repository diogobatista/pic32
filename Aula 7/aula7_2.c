#include<DETPIC32.h>

void init();

int main(void){

	init();

	while(1);

	return 0;
}

void init(){

	//Timer setup
	T3CONbits.TCKPS = 7;	//prescaler steup -> 2^n
	PR3 = 39062;		//divider constant
	TMR3 = 0;		//reset timer
	T3CONbits.TON = 1;	//enable timer

	//interrupts setup
	IFS0bits.T3IF = 0; 	  // Interrupt flag reset
	IPC3bits.T3IP = 2; 		// Set Interrupt Priority to 2
	IEC0bits.T3IE = 1; 		// Enable T3 interrupts
	
}

void _int_(12) isr_T3(void) // Replace VECTOR by the timer T3 vector number
{
	putChar('.');
	IFS0bits.T3IF = 0;
	// Reset T3 interrupt flag
}
