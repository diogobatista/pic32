#include <detpic32.h>

void init();
int half_flag = 0;


int main(void){
	
	init();
	EnableInterrupts();
	
	while(1)
	{

	}	
	

}

void _int_(12) isr_T3(void){
	
	if(half_flag == 1)
	{
	putChar('.');
	IFS0bits.T3IF = 0;
	half_flag = 0;
	}else
	{
	IFS0bits.T3IF = 0;
	half_flag = 1;	
	}
	
}

void init()
{
	// TIMER CONFIG
	T3CONbits.TCKPS = 7;  // Prescaler
	PR3 = 39062;
	TMR3 = 0;
	T3CONbits.TON = 1;	
	
	// INTERRUPTS
	
	IFS0bits.T3IF = 0; // Reset tha flag
	IPC3bits.T3IP = 3; // Interrupt priority
	IEC0bits.T3IE = 1; // Interrupts enabled
	
}
