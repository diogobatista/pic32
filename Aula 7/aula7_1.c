#include <detpic32.h>

void init();

int main(void){

	init();

	while(1){

		while(IFS0bits.T3IF==0);		//wait while timer is running
		IFS0bits.T3IF = 0;			//reset flag

		putChar('.');
	}
	
	return 0;
}

void init(){

	//Timer setup
	T3CONbits.TCKPS = 7;	//prescaler steup -> 2^n
	PR3 = 39062;		//divider constant
	TMR3 = 0;		//reset timer
	T3CONbits.TON = 1;	//enable timer
}


